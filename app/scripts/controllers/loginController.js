'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('LoginCtrl', ['$scope', '$http', '$state', '$cookieStore', 'serverIp', function ($scope, $http, $state, $cookieStore, serverIp) {
    $scope.password = null;
    $scope.email = null;
    $scope.invalidData = false;

    $scope.doLogin = function()
    {
		$http.post(
			serverIp + '/api/auth/login',
			{
				email: $scope.email,
				password: $scope.password
			}
		)
		.then(function(response){
			if(response.status == 422)
				$scope.invalidData = true;
			else
			{
				$cookieStore.put('user', response.data.data.user);
				$state.transitionTo('dashboard.home');
			}
		},function(error){
			console.log('error');
			console.log(error);
		});
    }

    $scope.doLogout = function()
    {
		$http.get(serverIp + '/api/auth/logout')
		.then(function(response){
			$state.transitionTo('login');
		},function(error){
			console.log('error');
			console.log(error);
		});
    }

}]);
'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('MainCtrl', function($scope,$position, $http, serverIp) {
  	$scope.incidents = '-';
  	$scope.quotationRequests = '-';

	$http.get(serverIp + '/api/incident').then(function(response){
		$scope.incidents = response.data.data.length;
	},function(error){
		console.log('error');
		console.log(error);
	});

	$http.get(serverIp + '/api/quotation-request').then(function(response){
		$scope.quotationRequests = response.data.data.length;
	},function(error){
		console.log('error');
		console.log(error);
	});

  });

'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('QuotationRequestsReportCtrl', ['$scope', '$http', '$timeout', 'serverIp', function ($scope, $http, $timeout, serverIp) {

    $scope.startDate = new Date().toLocaleFormat('%d-%m-%Y');;

    $scope.endDate = new Date().toLocaleFormat('%d-%m-%Y');

    $scope.hasSearches = false;

    $scope.hasResults = false;

    $scope.isLoading = false;

    $scope.dateError = false;

    $scope.requests = null;

    $scope.doSearch = function()
    {
        var endDateIsValid = moment($scope.endDate, "DD-MM-YYYY", true).isValid();
        var startDateIsValid = moment($scope.startDate, "DD-MM-YYYY", true).isValid();

        if (endDateIsValid && startDateIsValid) {

            $scope.hasSearches = true;

            $scope.isLoading = true;

            $scope.dateError = false;

            $http.post(
                serverIp + '/api/quotation-request/report',
                {
                    startDate: $scope.startDate,
                    endDate: $scope.endDate
                }
            )
            .then(function(response){
                if(response.status == 422) {
                    $scope.hasResults = false;
                    $scope.requests = null;
                }
                else
                {
                    if(response.data.data != null)
                    {
                        $scope.requests = response.data.data;
                        $scope.hasResults = true;
                        $scope.isLoading = false;
                    }else{
                        $scope.hasResults = false;
                        $scope.requests = null;
                        $scope.isLoading = false;                        
                    }
                }
            },function(error){
                console.log('error');
                console.log(error);
            });

        }else {

            $scope.dateError = true;
            console.log('fechas inválidas');
        }
    }

}]);
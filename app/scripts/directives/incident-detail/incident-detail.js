'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('incidentDetail',function(){
		return {
	        templateUrl:'scripts/directives/incident-detail/incident-detail.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', '$attrs', 'serverIp', function($scope, $http, $attrs, serverIp) {
	        	$scope.incidents = {};
	        	$attrs.$observe('incidentId', function(value) {
				    if (value) {
	        			$scope.incidentId = $attrs.incidentId;
	        			$http.get(serverIp + '/api/incident/show/'+$scope.incidentId).then(function(response){
							$scope.incident = response.data.data;
							$scope.map = { center: { latitude: $scope.incident.lat, longitude: $scope.incident.lng }, zoom: 14 };
							$scope.marker = {
						      id: $scope.incident.id,
						      coords: {
						        latitude: $scope.incident.lat,
						        longitude: $scope.incident.lng
						      },
						      options: { draggable: false }
						    };
						},function(error){
							console.log('error');
							console.log(error);
						});
				    }
				  });


		    }]
    	}
	});



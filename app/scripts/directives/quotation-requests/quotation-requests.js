'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('quotationRequests',function(){
		return {
	        templateUrl:'scripts/directives/quotation-requests/quotation-requests.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', 'serverIp', function($scope, $http, serverIp) {
	        	$scope.quotationRequests = {};

	        	$http.get(serverIp + '/api/quotation-request').then(function(response){
					$scope.quotationRequests = response.data.data;
				},function(error){
					console.log('error');
					console.log(error);
				});
		    }]
    	}
	});



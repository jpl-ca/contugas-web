'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('incidents',function(){
		return {
	        templateUrl:'scripts/directives/incidents/incidents.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', 'serverIp', function($scope, $http, serverIp) {
	        	$scope.incidents = {};

	        	$http.get(serverIp + '/api/incident').then(function(response){
					$scope.incidents = response.data.data;
				},function(error){
					console.log('error');
					console.log(error);
				});
		    }]
    	}
	});


